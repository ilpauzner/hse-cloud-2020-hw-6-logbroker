import asyncio
import csv
import glob
import json
import os
import ssl
import time

from aiofile import async_open
from aiohttp.client import ClientSession
from aiohttp.client_exceptions import ClientError
from fastapi import FastAPI, Request, Response
from fastapi_utils.tasks import repeat_every

CH_HOST = os.getenv('LOGBROKER_CH_HOST', 'localhost')
CH_USER = os.getenv('LOGBROKER_CH_USER')
CH_PASSWORD = os.getenv('LOGBROKER_CH_PASSWORD')
CH_PORT = int(os.getenv('LOGBROKER_CH_PORT', 8123))
CH_CERT_PATH = os.getenv('LOGBROKER_CH_CERT_PATH')

DELTA = 5


async def execute_query(query, data=None):
    url = f'http://{CH_HOST}:{CH_PORT}/'
    params = {
        'query': query.strip()
    }
    headers = {}
    if CH_USER is not None:
        headers['X-ClickHouse-User'] = CH_USER
        if CH_PASSWORD is not None:
            headers['X-ClickHouse-Key'] = CH_PASSWORD
    ssl_context = ssl.create_default_context(cafile=CH_CERT_PATH) if CH_CERT_PATH is not None else None

    async with ClientSession() as session:
        async with session.post(url, params=params, data=data, headers=headers, ssl=ssl_context) as resp:
            await resp.read()
            try:
                resp.raise_for_status()
                return resp, None
            except ClientError as e:
                return resp, {'error': str(e)}


app = FastAPI()


async def query_wrapper(query, data=None):
    res, err = await execute_query(query, data)
    if err is not None:
        return err
    return await res.text()


@app.get('/show_create_table')
async def show_create_table(table_name: str):
    resp = await query_wrapper(f'SHOW CREATE TABLE "{table_name}";')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp


@app.post('/write_log')
async def write_log(request: Request):
    body = await request.json()
    res = []
    for log_entry in body:
        log_format = log_entry['format']
        table_name = log_entry['table_name']
        rows = log_entry['rows']
        if log_format in ('list', 'json'):
            res.append(write_log_entry(log_format=log_format, table_name=table_name, rows=rows))
        else:
            res.append({'error': f'unknown format {log_entry.get("format")}, you must use list or json'})
    return res


@app.get('/healthcheck')
async def healthcheck():
    return Response(content='Ok', media_type='text/plain')


def get_filename(timestamp, log_format, table_name):
    return f'{timestamp}_{log_format}_{table_name}.log'


def write_log_entry(log_format, table_name, rows):
    timestamp = int(time.time())
    filename = get_filename(timestamp, log_format, table_name)
    print(f'Writing to {filename}')
    with open(filename, 'a') as f:
        if format == 'list':
            cwr = csv.writer(f, quoting=csv.QUOTE_ALL)
            cwr.writerows(rows)
        else:  # format == 'json'
            for row in rows:
                assert isinstance(row, dict)
                f.write(json.dumps(row))
                f.write('\n')
    return ""


@app.on_event("startup")
@repeat_every(seconds=1)
async def send_files():
    files = sorted(glob.glob('*_*_*.log'), key=os.path.getmtime)
    print(f'Available files: {files}')
    current_timestamp = int(time.time())
    for file in files:
        first_delimiter = file.find('_')
        second_delimiter = file[first_delimiter + 1:].find('_') + first_delimiter + 1
        last_delimiter = file.rfind('.')

        timestamp = int(file[:first_delimiter])
        if current_timestamp - timestamp >= DELTA:
            print(f'Sending {file} to clickhouse')
            log_format = file[first_delimiter + 1: second_delimiter]
            table_name = file[second_delimiter + 1: last_delimiter]
            async with async_open(file, 'r') as f:
                data = await f.read()
            if log_format == 'list':
                clickhouse_format = 'CSV'
            else:  # format == 'json
                clickhouse_format = 'JSONEachRow'
            task = asyncio.create_task(query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT {clickhouse_format}', data))
            done, pending = await asyncio.wait({task}, timeout=0.1)
            if task in done:
                os.remove(file)
        else:
            break
